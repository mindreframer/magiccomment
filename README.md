# Magiccomment

- `Apidocs`: central place for API schema description
- `Magiccomment`: updates API comments in-place if needed

## Usage:

Run `Magiccomment.run()` to update all API functions doc comments
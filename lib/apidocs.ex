defmodule Apidocs do
  def docs() do
    %{
      methods: %{
        "worlds.create": %{
          description: "Create a World"
        },
        "worlds.get": %{
          description: "Get a World by id"
        },
        "tenants.create": %{
          description: "Create a tenant"
        },
        "tenants.get": %{
          description: "Get a tenant"
        },
        "admin.users.invite": %{
          description: "Invite a user"
        },
        "admin.users.get": %{
          description: "Get a user"
        }
      }
    }
  end

  def for_method(m) when is_binary(m) do
    m |> String.to_atom() |> for_method()
  end

  def for_method(m) when is_atom(m) do
    docs() |> Map.get(:methods) |> Map.get(m)
  end
end

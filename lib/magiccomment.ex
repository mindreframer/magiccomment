defmodule Magiccomment do
  @apidocregex ~r/(?s) --apidoc:for(\s*) (.*?) --apidoc:end?/

  def run do
    files() |> Enum.each(&process_file/1)
  end

  def process_file(path) do
    IO.puts("--> start: #{path}")
    newcontent = handle_content(File.read!(path), path)
    File.write(path, "") # this prevents issues with excessive whitespace
    File.open(path, [:read, :write], fn file ->
      IO.write(file, newcontent)
      IO.write(file, "\n") # trailing newline
    end)
    IO.puts("--> end: #{path}")
  end

  def handle_content(content, path) do
    foundplaces = Regex.scan(@apidocregex, content)
    newcontent = replace_foundplaces(foundplaces, content)
    Code.format_string!(newcontent, file: path)
  end

  def replace_foundplaces(places, content) do
    Enum.reduce(places, content, fn [full, _, matched], acc ->
      method = matched |> getfirstword()
      mdocs = Apidocs.for_method(method)
      mdocs_str = inspect(mdocs)

      res = ~s{ --apidoc:for #{method}
      @doc  """
      #{mdocs_str}
      """
      @api true
      # --apidoc:end}
      String.replace(acc, full, res)
    end)
  end

  def getfirstword(str) do
    str |> String.split(" ", parts: 2) |> Enum.at(0) |> String.trim()
  end

  def files() do
    Path.wildcard("lib/**/*.ex") -- ["lib/magiccomment.ex"]
  end
end

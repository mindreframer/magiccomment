defmodule Worlds do
  # --apidoc:for worlds.create
  @doc """
  %{description: "Create a World"}
  """
  @api true
  # --apidoc:end
  def create(payload) do
    IO.inspect(payload)
  end

  # --apidoc:for worlds.get
  @doc """
  %{description: "Get a World by id"}
  """
  @api true
  # --apidoc:end
  def get(payload) do
    IO.inspect(payload)
  end
end

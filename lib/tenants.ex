defmodule Tenants do
  # --apidoc:for tenants.create
  @doc """
  %{description: "Create a tenant"}
  """
  @api true
  # --apidoc:end
  def create(payload) do
    IO.inspect(payload)
  end

  # --apidoc:for tenants.get
  @doc """
  %{description: "Get a tenant"}
  """
  @api true
  # --apidoc:end
  def get(payload) do
    IO.inspect(payload)
  end
end

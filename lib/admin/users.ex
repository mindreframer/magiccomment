defmodule Admin.Users do
  # --apidoc:for admin.users.invite
  @doc """
  %{description: "Invite a user"}
  """
  @api true
  # --apidoc:end
  def invite(payload) do
    IO.inspect(payload)
  end

  # --apidoc:for admin.users.get
  @doc """
  %{description: "Get a user"}
  """
  @api true
  # --apidoc:end
  def get(payload) do
    IO.inspect(payload)
  end
end

defmodule MagiccommentTest do
  use ExUnit.Case
  doctest Magiccomment

  describe "handle_content" do
    test "fills docs for multiple functions" do
      src = File.read!("test/samples/world.ex")
      res = Magiccomment.handle_content(src, "test/samples/world.ex")
      res = "#{res}"

      refute String.contains?(src, "Get a World by id")
      assert String.contains?(res, "Get a World by id")

      refute String.contains?(src, "Create a World")
      assert String.contains?(res, "Create a World")
    end

    test "strips whitespace" do
      src = File.read!("test/samples/world.ex")
      res = Magiccomment.handle_content(src, "test/samples/world.ex")
      res = "#{res}"

      assert String.contains?(src, "--apidoc:for            worlds.get")
      refute String.contains?(res, "--apidoc:for            worlds.get")
      assert String.contains?(res, "--apidoc:for worlds.get")

      assert String.contains?(src, "\n\n\n\n\n")
      refute String.contains?(res, "\n\n\n\n\n")
    end
  end
end

defmodule Worlds do
  ### Support multiple functions per file

  # --apidoc:for worlds.create
  # --apidoc:end
  def create(_) do
  end

  ### Support wide whitespaces
  # --apidoc:for            worlds.get
  # --apidoc:end
  def get(_) do
  end
end











